import axios from "axios";

export const fetchMovies = async (query: string) => {
	let response: any = {};
	try {
		response = await axios.get(`http://www.omdbapi.com/?apikey=61ae3795&s=${query}`);
		response = response && response.data && response.data.Search ? response.data.Search : [];
		let movieDirectorList: any = []
		await Promise.all(response.map(async (movie: any) => {
			const movieDetail = await fetchMovieDirector(movie.imdbID);
			movieDirectorList.push({title: movieDetail.Title, director: movieDetail.Director})
		}))
		return movieDirectorList
	} catch (error) {
		console.log("getting error while fetching movie data");
	}
	return response;
 
};

const fetchMovieDirector = async (movieId: string) => {
	let response: any = {};
	try {
		response = await axios.get(`http://www.omdbapi.com/?apikey=61ae3795&i=${movieId}`);
		response = response && response.data ? response.data : {}
	} catch (error) {
		console.log('error while fetching movie director')
	}
	return response
}