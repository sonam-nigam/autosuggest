import React, { Component } from 'react';
import './assets/css/Autosuggest.css';
import { fetchMovies } from './actions/AutosuggestAction'
import * as _ from "underscore";

import {Card, ListGroup, InputGroup, FormControl, Badge, Alert} from 'react-bootstrap'

declare namespace AutosuggestNamespace {
  export interface AutsuggestState {
    selectedList: any,
    optionList: any,
    openList: boolean,
    showNotFoundAlert: boolean
  }  
}

class Autosuggest extends Component<{}, AutosuggestNamespace.AutsuggestState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      optionList: [],
      selectedList: [],
      openList: false,
      showNotFoundAlert: false,
    };
  }
  queryString = "";
  container: any = React.createRef();

  fetchMovies = async (event: any) => {
    if (event && event.target && event.target.value && event.target.value.length > 2 ) {
      const queryString = event.target.value;
      this.queryString = queryString;
      let response = await fetchMovies(queryString);
      if (response.length > 0) {
        this.setState({
          optionList: response,
          openList: true,
          showNotFoundAlert: false,
        });
      } else {
        this.setState({
          showNotFoundAlert: true,
          openList: false,
          optionList: [],
        });
      }
    } else {
      this.setState({
        optionList: [],
        openList: false,
        showNotFoundAlert: false,
      });
    }
  };

  getDebounceResults = _.debounce(this.fetchMovies.bind(this), 100);

  fetchSearchResults = (event: any) => {
    event.persist();
    this.getDebounceResults(event);
  };

  saveSelectedOption = (title: string) => {
    if (this.state.selectedList.length >= 5) {
      alert("You can only select 5 movies");
    } else {
      if (this.state.selectedList.indexOf(title) === -1) {
        this.setState({ selectedList: [...this.state.selectedList, title] });
      }
    }
  };

  removeSelectedMovie = (title: string) => {
    let selectedMoviesList = this.state.selectedList;
    selectedMoviesList = selectedMoviesList.filter((e: string) => e !== title);
    this.setState({ selectedList: selectedMoviesList });
  };

  getHighlightedName = (name: string) => {
    let index: number = name.toLowerCase().indexOf(this.queryString);
    let counter: number = 1;
    while (index === -1) {
      index = name
        .toLowerCase()
        .indexOf(this.queryString.slice(0, this.queryString.length - counter));
      counter += 1;
    }
    let highlightedString = (
      <span>
        {name.slice(0, index)}
        <strong>{name.slice(index, index + this.queryString.length)}</strong>
        {name.slice(index + this.queryString.length)}
      </span>
    );
    return highlightedString;
  };

  getMovieList = () => {
    let movieList = this.state.optionList.map((name: any, index: number) => {
      return (
        <ListGroup.Item
          key={index}
          className="options"
          onClick={() => this.saveSelectedOption(name.title)}
        >
          <p className="mb-1">{this.getHighlightedName(name.title)}</p>
          <small>{name.director}</small>
        </ListGroup.Item>
      );
    });
    return movieList;
  };

  getSelectedMovie = () => {
    let selectedMovies = this.state.selectedList.map(
      (name: any, index: number) => {
        return (
          <InputGroup.Text key={index}>
            <Badge pill className="movie-badge">
              {name}
              <button
                type="button"
                className="close"
                aria-label="Close"
                onClick={() => this.removeSelectedMovie(name)}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </Badge>
          </InputGroup.Text>
        );
      }
    );
    return selectedMovies;
  };

  handleClickOutside = (event: any) => {
    if (
      this.container.current &&
      !this.container.current.contains(event.target)
    ) {
      this.setState({
        openList: false,
      });
    }
  };

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  render() {
    let moviesList = this.getMovieList();
    let selectedMovies = this.getSelectedMovie();
    return (
      <div className="autosuggest-div">
        <Card id="autosuggest-card" ref={this.container}>
          <InputGroup className="search-box">
            {selectedMovies.length >= 1 && (
              <InputGroup.Prepend>{selectedMovies}</InputGroup.Prepend>
            )}
            <FormControl
              className="shadow-none"
              placeholder="Search for movies"
              id="movies"
              onChange={this.fetchSearchResults}
            />
          </InputGroup>
          {this.state.openList && moviesList.length >= 1 && (
            <ListGroup variant="flush" >
              {moviesList}
            </ListGroup>
          )}
          {this.state.showNotFoundAlert && (
            <Alert variant="warning">
              No result found for {this.queryString} !
            </Alert>
          )}
        </Card>
      </div>
    );
  }
}
export default Autosuggest