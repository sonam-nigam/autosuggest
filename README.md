# README #
Movie title autosuggest library build on react.js for dweb

### Requirements ###
* requires the node version >= v12.4.0

### Steps to start the project ###
run the following commands in sequence:

* npm i
* npm run start

the project will start running on http://localhost:3000/